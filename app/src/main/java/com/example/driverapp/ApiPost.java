package com.example.driverapp;

import com.example.driverapp.Models.ChildLoc;
import com.example.driverapp.Models.ChildLocRes;
import com.example.driverapp.Models.ChildRecRes;
import com.example.driverapp.Models.ChildrenRecDatum;
import com.example.driverapp.Models.LoginRes;
import com.example.driverapp.Models.PickDropRes;
import com.example.driverapp.Models.PickupRecRes;
import com.example.driverapp.Models.SchoolsRes;
import com.example.driverapp.Models.ShowChildRes;
import com.example.driverapp.Models.SignupRes;
import com.example.driverapp.Models.TripNameRes;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiPost {

    @FormUrlEncoded
    @POST("driver_sign_up")
    Call<SignupRes> createUser(@Field("name") String name,
                               @Field("phone_no") String phone_no,
                               @Field("phone_no_2") String phone_no_2,
                               @Field("email") String email,
                               @Field("id_type") String id_type,
                               @Field("id_number") String id_number,
                               @Field("vehicle_reg") String vehicle_reg,
                               @Field("school_1") String school_1,
                               @Field("school_2") String school_2,
                               @Field("school_3") String school_3,
                               @Field("vehicle_status") String vehicle_status,
                               @Field("password") String password,
                               @Field("user_type") String user_type);

    @FormUrlEncoded
    @POST("driver_sign_in")
    Call<LoginRes> loginUser(@Field("phone_no") String phone_no,
                             @Field("password") String password);

    @POST("pickup")
    Call<PickDropRes> savePickupRec(@Body RequestBody body);

    @POST("dropoff")
    Call<PickDropRes> saveDropOffRec(@Body RequestBody body);

//    @Multipart
//    @POST("pickup")
//    Call<PickDropRes> uploadPickupRecord(
//            @PartMap Map<String, String> map);
//    @POST("show_records")
//    Call<ShowChildRes> showChildRecord(@Body ChildRecModel pickScanModel);

    @GET("on_going")
    Call<ShowChildRes> showChildRecord();

    @GET("all_schools")
    Call<SchoolsRes> getSchoolList();

    @POST("live_tracking")
    Call<ChildLocRes> sendChildLoc(@Body ChildLoc childLoc);

    @POST("get_data_parent")
    Call<ChildRecRes> showChildRec(@Body ChildRecRes childRecRes);

    @FormUrlEncoded
    @POST("trip_name")
    Call<TripNameRes> tripName(@Field("parent_id") String driver_id,
                               @Field("trip_name") String tripName);

    @POST("pickup_records_driver")
    Call<PickupRecRes> showPickupRec(@Body ChildrenRecDatum childrenRecDatum);

    @POST("dropoff_records_driver")
    Call<PickupRecRes> showDropoffRec(@Body ChildrenRecDatum childrenRecDatum);
}
