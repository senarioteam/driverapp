package com.example.driverapp.Fragments;

import android.Manifest;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.Datum;
import com.example.driverapp.Models.ShowChildRes;
import com.example.driverapp.R;
import com.example.driverapp.ShowRecordAdapter;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    private ShowRecordAdapter showRecordAdapter;
    TextView tvNoRecords;
    ImageView imgNoRecords;
    SwipeRefreshLayout swipeRefreshLayout;
    Button goLive;
    private final String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.swipe);
        tvNoRecords = view.findViewById(R.id.tvNoRecords);
        imgNoRecords = view.findViewById(R.id.imgNoRecords);
//        goLive = view.findViewById(R.id.live_stream);
//
//        goLive.setOnClickListener(v -> {
//            if (!hasPermissions(getContext(), PERMISSIONS)) {
//                ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 1);
//            } else {
//                startActivity(new Intent(getContext(), LiveStreamActivity.class));
//            }
//        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                childRecordList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        childRecordList();
        return view;
    }

    private void childRecordList() {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<ShowChildRes> call = apiPost.showChildRecord();
        call.enqueue(new Callback<ShowChildRes>() {
            @Override
            public void onResponse(Call<ShowChildRes> call, Response<ShowChildRes> response) {
                if (response.isSuccessful()) {
                    ShowChildRes resObj = response.body();
                    List<Datum> list = resObj.getData();

                    if (list != null) {
//                        goLive.setVisibility(View.VISIBLE);
                        showRecordAdapter = new ShowRecordAdapter((ArrayList<Datum>) list, getContext());
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                        Loading.dismiss();
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(showRecordAdapter);
                        showRecordAdapter.notifyDataSetChanged();
                    } else {
                        Loading.dismiss();
                        imgNoRecords.setVisibility(View.VISIBLE);
                        tvNoRecords.setVisibility(View.VISIBLE);
//                        goLive.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                    }

//                    for (Datum datum : list) {
//                        SharedPreferences.Editor editor = getContext().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE).edit();
//                        String idList = String.valueOf(datum.getUserId());
//                        Gson gson = new Gson();
//                        List<String> textList = new ArrayList<String>(Integer.parseInt(idList));
//                        String jsonText = gson.toJson(textList);
//
//                        editor.putString("sendtopickdrop", jsonText);
//                        editor.apply();
//                    }

//                    tvChildName.setText(list.get(0).getLocation());
//                    for (Datum datum : list) {
//                        tvChildName.append(datum.getLocation()+"\n\n");
//                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowChildRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(getContext(), "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}