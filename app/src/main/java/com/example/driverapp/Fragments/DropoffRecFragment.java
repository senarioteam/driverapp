package com.example.driverapp.Fragments;

import static com.example.driverapp.Activities.LoginActivity.MY_PREF;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.ChildrenRecDatum;
import com.example.driverapp.Models.PickupRecRes;
import com.example.driverapp.PickRecordAdapter;
import com.example.driverapp.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DropoffRecFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DropoffRecFragment extends Fragment {
    private RecyclerView recyclerView;
    private PickRecordAdapter pickRecordAdapter;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DropoffRecFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DropoffRecFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DropoffRecFragment newInstance(String param1, String param2) {
        DropoffRecFragment fragment = new DropoffRecFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dropoff_rec, container, false);
        recyclerView = view.findViewById(R.id.drop_rv);

        SharedPreferences preferences = getContext().getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String driverId = preferences.getString("driverIDKey", "abc");
        childRecordList(driverId);
        return view;
    }

    private void childRecordList(String driver_id) {
        Loading.show(getActivity());
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildrenRecDatum childrenRecDatum = new ChildrenRecDatum(Integer.parseInt(driver_id));
        Call<PickupRecRes> call = apiPost.showDropoffRec(childrenRecDatum);
        call.enqueue(new Callback<PickupRecRes>() {
            @Override
            public void onResponse(Call<PickupRecRes> call, Response<PickupRecRes> response) {
                if (response.isSuccessful()) {
                    PickupRecRes resObj = response.body();
                    List<ChildrenRecDatum> list = resObj.getData();
                    pickRecordAdapter = new PickRecordAdapter((ArrayList<ChildrenRecDatum>) list, getContext(), 1);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);
                    Loading.dismiss();
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(pickRecordAdapter);
                    pickRecordAdapter.notifyDataSetChanged();

                } else {
                    Loading.dismiss();
                    Toast.makeText(getContext(), "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickupRecRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getContext(), "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(getContext(), "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}