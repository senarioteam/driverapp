package com.example.driverapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("childs_name")
    @Expose
    private String childsName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("trip_time")
    @Expose
    private Object tripTime;
    @SerializedName("COUNT(status)")
    @Expose
    private Integer cOUNTStatus;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getChildsName() {
        return childsName;
    }

    public void setChildsName(String childsName) {
        this.childsName = childsName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getTripTime() {
        return tripTime;
    }

    public void setTripTime(Object tripTime) {
        this.tripTime = tripTime;
    }

    public Integer getCOUNTStatus() {
        return cOUNTStatus;
    }

    public void setCOUNTStatus(Integer cOUNTStatus) {
        this.cOUNTStatus = cOUNTStatus;
    }

}
