package com.example.driverapp.Models;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("message")
    private String message;
    @SerializedName("email")
    private String email;
    @SerializedName("password")
    private String password;

    public LoginModel(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
