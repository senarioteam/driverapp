package com.example.driverapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SchoolsDatum {

    @SerializedName("Schools_name")
    @Expose
    private String schoolsName;

    public String getSchoolsName() {
        return schoolsName;
    }

    public void setSchoolsName(String schoolsName) {
        this.schoolsName = schoolsName;
    }

}
