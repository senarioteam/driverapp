package com.example.driverapp.Models;

import com.google.gson.annotations.SerializedName;

public class ScanInputs {
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("location")
    private String location;
    @SerializedName("status")
    private String status;

    public ScanInputs(String user_id, String location, String status) {
        this.user_id = user_id;
        this.location = location;
        this.status = status;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
