package com.example.driverapp.Models;

import android.os.Bundle;

import com.example.driverapp.Fragments.HomeFragment;

public class HomeFragmentClass extends HomeFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null){
            getActivity().getSupportFragmentManager().beginTransaction()
                    .add(android.R.id.content, new HomeFragment ()).commit();}
    }

    public HomeFragmentClass() {
    }
}
