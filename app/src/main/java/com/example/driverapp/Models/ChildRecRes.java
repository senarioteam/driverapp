package com.example.driverapp.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChildRecRes {
    @SerializedName("id")
    private String id;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<ChildRecord> data = null;

    public ChildRecRes(String id) {
        this.id = id;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<ChildRecord> getData() {
        return data;
    }

    public void setData(List<ChildRecord> data) {
        this.data = data;
    }
}
