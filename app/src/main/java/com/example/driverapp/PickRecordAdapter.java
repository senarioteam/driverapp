package com.example.driverapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.driverapp.Models.ChildrenRecDatum;

import java.util.ArrayList;

public class PickRecordAdapter extends RecyclerView.Adapter<PickRecordAdapter.ViewHolder> {
    private ArrayList<ChildrenRecDatum> recDatumArrayList = new ArrayList<>();
    private Context context;
    private int viewType;

    public PickRecordAdapter(ArrayList<ChildrenRecDatum> recDatumArrayList, Context context, int viewType) {
        this.recDatumArrayList = recDatumArrayList;
        this.context = context;
        this.viewType = viewType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.child_rec_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(recDatumArrayList.get(position).getChildsName());
        holder.location.setText(recDatumArrayList.get(position).getLocation());
        holder.status.setText(recDatumArrayList.get(position).getStatus());
        holder.create_time.setText(recDatumArrayList.get(position).getCreatedAt());
        if (viewType == 0) {
            holder.tripLin.setVisibility(View.GONE);
            holder.view.setVisibility(View.GONE);
        } else {
            holder.trip_time.setText(recDatumArrayList.get(position).getUpdatedAt());
        }
    }

    @Override
    public int getItemCount() {
        return recDatumArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, location, status, create_time, trip_time;
        LinearLayout tripLin;
        View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            location = itemView.findViewById(R.id.location);
            status = itemView.findViewById(R.id.status);
            create_time = itemView.findViewById(R.id.createdat);
            trip_time = itemView.findViewById(R.id.trip_time);
            tripLin = itemView.findViewById(R.id.trip_lin);
            view = itemView.findViewById(R.id.trip_view_line);
        }
    }
}
