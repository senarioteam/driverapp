package com.example.driverapp;

import static com.example.driverapp.Activities.LoginActivity.MY_PREF;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.example.driverapp.Models.ChildLoc;
import com.example.driverapp.Models.ChildLocRes;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyService extends Service {
    FusedLocationProviderClient fusedLocationProviderClient;
    Handler handler = new Handler();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        startRepeatingTask();
        createNotificationChannel();

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notification = new NotificationCompat.Builder(this, "ChannelId1")
                .setContentTitle("Location Status")
                .setContentText("Your current location is sending to parents")
                .setSmallIcon(R.drawable.driver)
                .setContentIntent(pendingIntent).build();

        startForeground(1, notification);

        return START_REDELIVER_INTENT;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(
                    "ChannelId1", "Foreground notification", NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(notificationChannel);

        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        stopForeground(true);
        stopSelf();
        super.onDestroy();
    }

    private void callLocation(double longitude, double latitude, String id, Context context) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildLoc childLoc = new ChildLoc(longitude, latitude, id);
        Call<ChildLocRes> call = apiPost.sendChildLoc(childLoc);
        call.enqueue(new Callback<ChildLocRes>() {
            @Override
            public void onResponse(Call<ChildLocRes> call, Response<ChildLocRes> response) {
                if (response.isSuccessful()) {
                    ChildLocRes resObj = response.body();
                    if (resObj.getSuccess().equals("true")) {
                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChildLocRes> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(MyService.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(MyService.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void sendLocation(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(MY_PREF, Context.MODE_PRIVATE);
        String driverId = preferences.getString("driverIDKey", "abv");
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull @NotNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null) {
                        try {
                            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(
                                    location.getLatitude(), location.getLongitude(), 1);
                            callLocation(addresses.get(0).getLongitude(), addresses.get(0).getLatitude(), driverId, context);
//                            Toast.makeText(context, "log :" + addresses.get(0).getLongitude()
//                                    + "Lat: " + addresses.get(0).getLatitude(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } else {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 44);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            sendLocation(MyService.this);
            handler.postDelayed(runnable, 80000);
        }
    };

    void startRepeatingTask() {
        runnable.run();
    }
}
