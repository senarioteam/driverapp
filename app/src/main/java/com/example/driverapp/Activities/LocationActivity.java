package com.example.driverapp.Activities;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.example.driverapp.Activities.LoginActivity.MY_PREF;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.Datum;
import com.example.driverapp.Models.PickDropRes;
import com.example.driverapp.Models.ShowChildRes;
import com.example.driverapp.MyService;
import com.example.driverapp.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationActivity extends AppCompatActivity {
    private static final int REQUEST_CODE = 123;
    private static final int REQUEST_PERMISSIONS_SETTING = 1;
    Button btnSchool, btnHome, btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        allowLocPermission();

        btnSchool = findViewById(R.id.btnSchool);
        btnHome = findViewById(R.id.btnHome);
        btnCancel = findViewById(R.id.btnCancel);

        showScannedRecord();
        checkGPS();

        SharedPreferences preferences = getSharedPreferences(MY_PREF, MODE_PRIVATE);
        String driverId = preferences.getString("driverIDKey", "abc");
        String driverName = preferences.getString("name", "bc");
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);

        String pickkey = getIntent().getStringExtra("pickKey");
        String dropkey = getIntent().getStringExtra("dropKey");
        String userId = getIntent().getStringExtra("idNextKey");
        Intent intent = new Intent(this, MyService.class);
        btnCancel.setOnClickListener(view -> {
            startActivity(new Intent(LocationActivity.this, NavMainActivity.class));
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isServicesOK()) {

                    if (pickkey != null) {
//                    startRepeatingTask();
                        savePickupRecords(userId, "Home", "Picked up", driverId);
                        if (isMyServiceRunning(MyService.class)) {
                            Log.d("isservice", "running");
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                startForegroundService(intent);
                            } else {
                                startService(intent);
                            }
                        }
                    } else if (dropkey.equals("Dropped off")) {
//                    stopRepeatingTask();
                        saveDropOffRecords(userId, "Home", "Dropped off", driverName + formattedDate, driverId);
                        if (isMyServiceRunning(MyService.class)) {
                            LocationActivity.this.stopService(intent);
                        }
                    }
                }
            }
        });
        btnSchool.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isServicesOK()) {

                    if (pickkey != null) {
//                    startRepeatingTask();
                        savePickupRecords(userId, "School", "Picked up", driverId);
                        if (isMyServiceRunning(MyService.class)) {
                            Log.d("isservice", "running");
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                startForegroundService(intent);
                            } else {
                                startService(intent);
                            }
                        }
                    } else if (dropkey.equals("Dropped off")) {
//                    stopRepeatingTask();
                        saveDropOffRecords(userId, "School", "Dropped off", driverName + formattedDate, driverId);
                        if (isMyServiceRunning(MyService.class)) {
                            LocationActivity.this.stopService(intent);
                        }
                    }
                }
            }
        });
    }

    private void savePickupRecords(String id, String location, String status, String driverId) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("id", id)
                .addFormDataPart("location", location)
                .addFormDataPart("status", status)
                .addFormDataPart("driver_id", driverId)
                .build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
//        ScanInputs scanInputs = new ScanInputs(id, location, status);
        Call<PickDropRes> call = apiPost.savePickupRec(requestBody);
        call.enqueue(new Callback<PickDropRes>() {
            @Override
            public void onResponse(Call<PickDropRes> call, Response<PickDropRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
//                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREF, MODE_PRIVATE).edit();
//                        editor.putString("pickklocKey", "school");
//                        editor.apply();

                    Intent intent = new Intent(LocationActivity.this, NavMainActivity.class);
                    startActivity(intent);
                } else {
                    Loading.dismiss();
                    Toast.makeText(LocationActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickDropRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(LocationActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(LocationActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveDropOffRecords(String id, String location, String status, String trip_name, String driver_id) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
//        Map<String, String> map = new HashMap<>();
//        map.put("id","10");
//        map.put("location","school");
//        map.put("status","picked up");
        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("id", id)
                .addFormDataPart("location", location)
                .addFormDataPart("status", status)
                .addFormDataPart("trip_name", trip_name)
                .build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
//        ScanInputs scanInputs = new ScanInputs(id, location, status);
        Call<PickDropRes> call = apiPost.saveDropOffRec(requestBody);
        call.enqueue(new Callback<PickDropRes>() {
            @Override
            public void onResponse(Call<PickDropRes> call, Response<PickDropRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
//                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREF, MODE_PRIVATE).edit();
//                        editor.putString("pickklocKey", "school");
//                        editor.apply();
                    Intent intent = new Intent(LocationActivity.this, NavMainActivity.class);
                    startActivity(intent);
                } else {
                    Loading.dismiss();
                    Toast.makeText(LocationActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PickDropRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(LocationActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(LocationActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showScannedRecord() {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<ShowChildRes> call = apiPost.showChildRecord();
        call.enqueue(new Callback<ShowChildRes>() {
            @Override
            public void onResponse(Call<ShowChildRes> call, Response<ShowChildRes> response) {
                if (response.isSuccessful()) {
                    ShowChildRes resObj = response.body();
                    List<Datum> list = resObj.getData();
                    String scanId = getIntent().getStringExtra("idNextKey");
                    if (list != null) {

                        for (Datum datum : list) {
                            String ids = datum.getUserId().toString();
                            String status = datum.getStatus();
                            String loc = datum.getLocation();
                            if (ids.contains(scanId) && status.equals("On going")) {
                                if (loc.equals("Home")) {
                                    Loading.dismiss();
                                    btnHome.setVisibility(View.GONE);
                                    btnSchool.setVisibility(View.VISIBLE);
                                } else {
                                    Loading.dismiss();
                                    btnHome.setVisibility(View.VISIBLE);
                                    btnSchool.setVisibility(View.GONE);
                                }
                            } else if (!ids.contains(scanId)) {
                                Loading.dismiss();
                                btnHome.setVisibility(View.VISIBLE);
                                btnSchool.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        Loading.dismiss();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(LocationActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowChildRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(LocationActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(LocationActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void allowLocPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions((Activity) this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull @NotNull String[] permissions, @NonNull @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            for (int i = 0; i < permissions.length; i++) {
                String per = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    boolean showRationale = shouldShowRequestPermissionRationale(per);
                    if (!showRationale) {
                        //user click on never ask again
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Location Permission")
                                .setMessage("You must allow the location permission for using this app"
                                        + "\n\n" + "Now follow below steps" + "\n\n" +
                                        "Open settings from below button" + "\n" +
                                        "Click on permissions" + "\n" +
                                        "Allow access for location")
                                .setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivityForResult(intent, REQUEST_PERMISSIONS_SETTING);
                                    }
                                }).create().show();
                    } else {
                        ActivityCompat.requestPermissions(LocationActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
                    }
                } else {

                }
            }

        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void checkGPS() {
        LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        getLocationAccess();
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();


        } else {
            getLocationAccess();
        }
    }

    private void buildAlertMessageNoGps() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setMessage("Your GPS Location seems to be disabled, You have to enable it, Do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void getLocationAccess() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 111);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 222);

        }
    }

    public boolean isServicesOK() {
        Log.d(TAG, "isServicesOK:  checking google services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (available == ConnectionResult.SUCCESS) {
            Log.d(TAG, "isServicesOK: google play services is working");
            return true;
        } else if (GoogleApiAvailability.getInstance().isUserResolvableError(available)) {
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(this, available, 27);
            dialog.show();
        } else {
            Toast.makeText(LocationActivity.this, "You cannot make map request", Toast.LENGTH_SHORT).show();
        }
        return false;
    }
}