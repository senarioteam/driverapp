package com.example.driverapp.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.AuthSession;
import com.example.driverapp.Models.LoginRes;
import com.example.driverapp.Models.User;
import com.example.driverapp.Models.UserSession;
import com.example.driverapp.R;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    public static final String MY_PREF = "my pref";
    private EditText phone, password;
    private Button btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();

        String signupToHere = getIntent().getStringExtra("signupKey");
        if (signupToHere != null && signupToHere.equals("yes")) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle("Account status");
            alertDialog.setMessage("Thank you for registering on MAAPE. Your details have been saved successfully. You are required to send your ID Copy and Drivers License copy to drivers@maape.com. Your account will be activated once the registration details have been verified");
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }

        initViews();
    }

    private void initViews() {
        phone = findViewById(R.id.phone_no);
        password = findViewById(R.id.signin_password);
        btnlogin = findViewById(R.id.loginbutton);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phonetext = phone.getText().toString();
                String passwordtext = password.getText().toString();
                if (TextUtils.isEmpty(phonetext)) {
                    phone.setError("enter your phone no");
                    return;
                } else {
                    phone.setError(null);
                }
                if (TextUtils.isEmpty(passwordtext)) {
                    password.setError("enter your password");
                    return;
                } else {
                    password.setError(null);
                }
                if (passwordtext.length() < 8) {
                    password.setError("password too short");
                    return;
                } else {
                    password.setError(null);
                }
                UserSession user = new UserSession(phonetext, passwordtext);
                AuthSession authSession = new AuthSession(LoginActivity.this);
                authSession.saveSession(user);
                loginUser(phonetext, passwordtext);
            }
        });
    }

    private void loginUser(String email, String password) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<LoginRes> call = apiPost.loginUser(email, password);
        call.enqueue(new Callback<LoginRes>() {
            @Override
            public void onResponse(Call<LoginRes> call, Response<LoginRes> response) {
                if (response.isSuccessful()) {
                    LoginRes resObj = response.body();
                    List<User> list = Collections.singletonList(resObj.getUser());
                    int driverid = list.get(0).getId();
                    String drivername = list.get(0).getName();

                    SharedPreferences preferences = getSharedPreferences(MY_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("driverIDKey", String.valueOf(driverid));
                    editor.putString("name", drivername);
                    editor.apply();

                    if (resObj.getMessage().equals("Signed in successfully")) {
                        Loading.dismiss();
                        Toast.makeText(LoginActivity.this, "Logged in Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, NavMainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(LoginActivity.this, response.body().getMessage().toString().trim(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void moveToSignup(View view) {
        startActivity(new Intent(LoginActivity.this, SignupActivity.class));
        finish();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        AuthSession authSession = new AuthSession(LoginActivity.this);
//        String email = authSession.getSession();
//        if (!email.equals("abc")) {
//            startActivity(new Intent(LoginActivity.this, NavMainActivity.class));
//            finish();
//        } else {
//        }
//    }
}