package com.example.driverapp.Activities;

import static com.example.driverapp.Activities.LoginActivity.MY_PREF;

import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.driverapp.R;
import com.example.driverapp.Utils.PathUtils;
import com.google.android.material.navigation.NavigationView;
import com.pedro.encoder.input.video.CameraHelper;
import com.pedro.encoder.input.video.CameraOpenException;
import com.pedro.rtmp.utils.ConnectCheckerRtmp;
import com.pedro.rtplibrary.rtmp.RtmpCamera1;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LiveStreamActivity extends AppCompatActivity
        implements Button.OnClickListener, ConnectCheckerRtmp, SurfaceHolder.Callback,
        View.OnTouchListener {
    private Integer[] orientations = new Integer[]{0, 90, 180, 270};
    private RtmpCamera1 rtmpCamera1;
    private Button bStartStop, bRecord;
    private EditText etUrl;
    private String currentDateAndTime = "";
    private File folder;
    //options menu
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private RadioGroup rgChannel;
    private Spinner spResolution;
    private CheckBox cbEchoCanceler, cbNoiseSuppressor;
    private EditText etVideoBitrate, etFps, etAudioBitrate, etSampleRate, etWowzaUser,
            etWowzaPassword;
    private String lastVideoBitrate;
    private TextView tvBitrate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_live_stream);
        folder = PathUtils.getRecordPath(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().hide();

        SurfaceView surfaceView = findViewById(R.id.surfaceView);
        surfaceView.getHolder().addCallback(this);
        surfaceView.setOnTouchListener(this);
        rtmpCamera1 = new RtmpCamera1(surfaceView, this);
        prepareOptionsMenuViews();
        tvBitrate = findViewById(R.id.tv_bitrate);
        etUrl = findViewById(R.id.et_rtp_url);
        etUrl.setHint(R.string.hint_rtmp);
        bStartStop = findViewById(R.id.b_start_stop);
        bStartStop.setOnClickListener(this);
//        bRecord = findViewById(R.id.b_record);
//        bRecord.setOnClickListener(this);
        Button switchCamera = findViewById(R.id.switch_camera);
        switchCamera.setOnClickListener(this);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }


    private void prepareOptionsMenuViews() {
        drawerLayout = findViewById(R.id.activity_custom);
        navigationView = findViewById(R.id.nv_rtp);

        navigationView.inflateMenu(R.menu.options_rtmp);
        navigationView.setVisibility(View.GONE);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.rtmp_streamer,
                R.string.rtmp_streamer) {

            public void onDrawerOpened(View drawerView) {
                actionBarDrawerToggle.syncState();
                lastVideoBitrate = etVideoBitrate.getText().toString();
            }

            public void onDrawerClosed(View view) {
                actionBarDrawerToggle.syncState();
                if (lastVideoBitrate != null && !lastVideoBitrate.equals(
                        etVideoBitrate.getText().toString()) && rtmpCamera1.isStreaming()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        int bitrate = Integer.parseInt(etVideoBitrate.getText().toString()) * 1024;
                        rtmpCamera1.setVideoBitrateOnFly(bitrate);
                        Toast.makeText(LiveStreamActivity.this, "New bitrate: " + bitrate, Toast.LENGTH_SHORT).
                                show();
                    } else {
                        Toast.makeText(LiveStreamActivity.this, "Bitrate on fly ignored, Required min API 19",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        //checkboxs
        cbEchoCanceler =
                (CheckBox) navigationView.getMenu().findItem(R.id.cb_echo_canceler).getActionView();
        cbNoiseSuppressor =
                (CheckBox) navigationView.getMenu().findItem(R.id.cb_noise_suppressor).getActionView();
        //radiobuttons
        RadioButton rbTcp =
                (RadioButton) navigationView.getMenu().findItem(R.id.rb_tcp).getActionView();
        rgChannel = (RadioGroup) navigationView.getMenu().findItem(R.id.channel).getActionView();
        rbTcp.setChecked(true);
        //spinners
        spResolution = (Spinner) navigationView.getMenu().findItem(R.id.sp_resolution).getActionView();

        ArrayAdapter<Integer> orientationAdapter =
                new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item);
        orientationAdapter.addAll(orientations);

        ArrayAdapter<String> resolutionAdapter =
                new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item);
        List<String> list = new ArrayList<>();
        for (Camera.Size size : rtmpCamera1.getResolutionsBack()) {
            list.add(size.width + "X" + size.height);
        }
        resolutionAdapter.addAll(list);
        spResolution.setAdapter(resolutionAdapter);
        //edittexts
        etVideoBitrate =
                (EditText) navigationView.getMenu().findItem(R.id.et_video_bitrate).getActionView();
        etFps = (EditText) navigationView.getMenu().findItem(R.id.et_fps).getActionView();
        etAudioBitrate =
                (EditText) navigationView.getMenu().findItem(R.id.et_audio_bitrate).getActionView();
        etSampleRate = (EditText) navigationView.getMenu().findItem(R.id.et_samplerate).getActionView();
        etVideoBitrate.setText("2500");
        etFps.setText("30");
        etAudioBitrate.setText("128");
        etSampleRate.setText("44100");
        etWowzaUser = (EditText) navigationView.getMenu().findItem(R.id.et_user).getActionView();
        etWowzaPassword =
                (EditText) navigationView.getMenu().findItem(R.id.et_password).getActionView();

    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                if (!drawerLayout.isDrawerOpen(GravityCompat.START)) {
//                    drawerLayout.openDrawer(GravityCompat.START);
//                } else {
//                    drawerLayout.closeDrawer(GravityCompat.START);
//                }
//                return true;
//            case R.id.microphone:
//                if (!rtmpCamera1.isAudioMuted()) {
//                    item.setIcon(getResources().getDrawable(R.drawable.ic_home));
//                    rtmpCamera1.disableAudio();
//                } else {
//                    item.setIcon(getResources().getDrawable(R.drawable.ic_home));
//                    rtmpCamera1.enableAudio();
//                }
//                return true;
//            default:
//                return false;
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_start_stop:
                Log.d("TAG_R", "b_start_stop: ");
                if (!rtmpCamera1.isStreaming()) {
                    bStartStop.setText(getResources().getString(R.string.stop_button));
                    String user = etWowzaUser.getText().toString();
                    String password = etWowzaPassword.getText().toString();
                    if (!user.isEmpty() && !password.isEmpty()) {
                        rtmpCamera1.setAuthorization(user, password);
                    }
                    if (rtmpCamera1.isRecording() || prepareEncoders()) {
                        SharedPreferences preferences = getSharedPreferences(MY_PREF, MODE_PRIVATE);
                        String driverName = preferences.getString("name", "ok");
                        rtmpCamera1.startStream("rtmp://162.240.21.77:1935/live/" + driverName);
                    } else {
                        //If you see this all time when you start stream,
                        //it is because your encoder device dont support the configuration
                        //in video encoder maybe color format.
                        //If you have more encoder go to VideoEncoder or AudioEncoder class,
                        //change encoder and try
                        Toast.makeText(this, "Error preparing stream, This device cant do it",
                                Toast.LENGTH_SHORT).show();
                        bStartStop.setText(getResources().getString(R.string.start_button));
                    }
                } else {
                    bStartStop.setText(getResources().getString(R.string.start_button));
                    rtmpCamera1.stopStream();
                }
                break;
//            case R.id.b_record:
//                Log.d("TAG_R", "b_start_stop: ");
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
//                    if (!rtmpCamera1.isRecording()) {
//                        try {
//                            if (!folder.exists()) {
//                                folder.mkdir();
//                            }
//                            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault());
//                            currentDateAndTime = sdf.format(new Date());
//                            if (!rtmpCamera1.isStreaming()) {
//                                if (prepareEncoders()) {
//                                    rtmpCamera1.startRecord(
//                                            folder.getAbsolutePath() + "/" + currentDateAndTime + ".mp4");
//                                    bRecord.setText(R.string.stop_record);
//                                    Toast.makeText(this, "Recording... ", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(this, "Error preparing stream, This device cant do it",
//                                            Toast.LENGTH_SHORT).show();
//                                }
//                            } else {
//                                rtmpCamera1.startRecord(
//                                        folder.getAbsolutePath() + "/" + currentDateAndTime + ".mp4");
//                                bRecord.setText(R.string.stop_record);
//                                Toast.makeText(this, "Recording... ", Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (IOException e) {
//                            rtmpCamera1.stopRecord();
//                            bRecord.setText(R.string.start_record);
//                            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                        }
//                    } else {
//                        rtmpCamera1.stopRecord();
//                        bRecord.setText(R.string.start_record);
//                        Toast.makeText(this,
//                                "file " + currentDateAndTime + ".mp4 saved in " + folder.getAbsolutePath(),
//                                Toast.LENGTH_SHORT).show();
//                        currentDateAndTime = "";
//                    }
//                } else {
//                    Toast.makeText(this, "You need min JELLY_BEAN_MR2(API 18) for do it...",
//                            Toast.LENGTH_SHORT).show();
//                }
//                break;
            case R.id.switch_camera:
                try {
                    rtmpCamera1.switchCamera();
                } catch (final CameraOpenException e) {
                    Toast.makeText(LiveStreamActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }

    private boolean prepareEncoders() {
        Camera.Size resolution =
                rtmpCamera1.getResolutionsBack().get(spResolution.getSelectedItemPosition());
        int width = resolution.width;
        int height = resolution.height;
        return rtmpCamera1.prepareVideo(width, height, Integer.parseInt(etFps.getText().toString()),
                Integer.parseInt(etVideoBitrate.getText().toString()) * 1024,
                CameraHelper.getCameraOrientation(this)) && rtmpCamera1.prepareAudio(
                Integer.parseInt(etAudioBitrate.getText().toString()) * 1024,
                Integer.parseInt(etSampleRate.getText().toString()),
                rgChannel.getCheckedRadioButtonId() == R.id.rb_stereo, cbEchoCanceler.isChecked(),
                cbNoiseSuppressor.isChecked());
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        rtmpCamera1.startPreview();
        // optionally:
        //rtmpCamera1.startPreview(CameraHelper.Facing.BACK);
        //or
        //rtmpCamera1.startPreview(CameraHelper.Facing.FRONT);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && rtmpCamera1.isRecording()) {
            rtmpCamera1.stopRecord();
            bRecord.setText(R.string.start_record);
            Toast.makeText(this,
                    "file " + currentDateAndTime + ".mp4 saved in " + folder.getAbsolutePath(),
                    Toast.LENGTH_SHORT).show();
            currentDateAndTime = "";
        }
        if (rtmpCamera1.isStreaming()) {
            rtmpCamera1.stopStream();
            bStartStop.setText(getResources().getString(R.string.start_button));
        }
        rtmpCamera1.stopPreview();
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (motionEvent.getPointerCount() > 1) {
            if (action == MotionEvent.ACTION_MOVE) {
                rtmpCamera1.setZoom(motionEvent);
            }
        } else if (action == MotionEvent.ACTION_DOWN) {
            rtmpCamera1.tapToFocus(view, motionEvent);
        }
        return true;
    }

    @Override
    public void onAuthErrorRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LiveStreamActivity.this, "Auth error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAuthSuccessRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LiveStreamActivity.this, "Auth success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onConnectionFailedRtmp(@NonNull String s) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LiveStreamActivity.this, "Connection failed. " + s, Toast.LENGTH_SHORT)
                        .show();
                rtmpCamera1.stopStream();
                bStartStop.setText(getResources().getString(R.string.start_button));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                        && rtmpCamera1.isRecording()) {
                    rtmpCamera1.stopRecord();
                    bRecord.setText(R.string.start_record);
                    Toast.makeText(LiveStreamActivity.this,
                            "file " + currentDateAndTime + ".mp4 saved in " + folder.getAbsolutePath(),
                            Toast.LENGTH_SHORT).show();
                    currentDateAndTime = "";
                }
            }
        });
    }

    @Override
    public void onConnectionStartedRtmp(@NonNull String s) {

    }

    @Override
    public void onConnectionSuccessRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LiveStreamActivity.this, "Connection success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDisconnectRtmp() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LiveStreamActivity.this, "Disconnected", Toast.LENGTH_SHORT).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                        && rtmpCamera1.isRecording()) {
                    rtmpCamera1.stopRecord();
                    bRecord.setText(R.string.start_record);
                    Toast.makeText(LiveStreamActivity.this,
                            "file " + currentDateAndTime + ".mp4 saved in " + folder.getAbsolutePath(),
                            Toast.LENGTH_SHORT).show();
                    currentDateAndTime = "";
                }
            }
        });
    }

    @Override
    public void onNewBitrateRtmp(long l) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvBitrate.setText(l + " bps");
            }
        });
    }
}