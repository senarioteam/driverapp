package com.example.driverapp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.SchoolsDatum;
import com.example.driverapp.Models.SchoolsRes;
import com.example.driverapp.Models.SignupRes;
import com.example.driverapp.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SignupActivity extends AppCompatActivity {

    private Button signup;
    private AutoCompleteTextView school1, school2, school3, vehicle_status;
    public static String emailpattern = "[a-zA-Z0-9._-]+@[a-z]+.[a-z]+";
    EditText driver_name, mobile1, mobile2, driver_email, id_type, id_number, vehicle_reg_no, password;
    private static final String[] COUNTRIES = new String[]{
            "beacon house", "dps", "fc", "tatatatata", "gc", "Nairobi Montessori Karen"
    };
    private static final String[] VEH_STATUS = new String[]{
            "Owned", "Rented", "Leased", "Loaned"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        getSupportActionBar().hide();

        initViews();
        clickListeners();
    }

    private void initViews() {
        driver_name = findViewById(R.id.driver_name);
        mobile1 = findViewById(R.id.mobile1);
        mobile2 = findViewById(R.id.mobile2);
        driver_email = findViewById(R.id.driver_email);
        id_type = findViewById(R.id.id_type);
        id_number = findViewById(R.id.id_number);
        vehicle_reg_no = findViewById(R.id.vehicle_reg_no);
        school1 = findViewById(R.id.school1);
        school2 = findViewById(R.id.school2);
        school3 = findViewById(R.id.school3);
        vehicle_status = findViewById(R.id.vehicle_status);
        password = findViewById(R.id.signup_password);
        signup = findViewById(R.id.signupbutton);
        school1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    getSchools();
                }
            }
        });
        school2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this,
                            android.R.layout.simple_list_item_1, COUNTRIES);
                    school2.setAdapter(adapter);
                    school2.showDropDown();
                }
            }
        });
        school3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this,
                            android.R.layout.simple_list_item_1, COUNTRIES);
                    school3.setAdapter(adapter);
                    school3.showDropDown();
                }
            }
        });
        vehicle_status.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this,
                            android.R.layout.simple_list_item_1, VEH_STATUS);
                    vehicle_status.setAdapter(adapter);
                    vehicle_status.showDropDown();
                }
            }
        });
    }

    private void clickListeners() {
        signup.setOnClickListener(v -> {
            String driverName = driver_name.getText().toString();
            String mobileno1 = mobile1.getText().toString();
            String mobileno2 = mobile2.getText().toString();
            String driverEmail = driver_email.getText().toString();
            String idType = id_type.getText().toString();
            String idNumber = id_number.getText().toString();
            String vehicleReg = vehicle_reg_no.getText().toString();
            String schoolno1 = school1.getText().toString();
            String schoolno2 = school2.getText().toString();
            String schoolno3 = school3.getText().toString();
            String vehicle_stat = vehicle_status.getText().toString();
            String pass = password.getText().toString();
            if (!fieldErrorExist()) {
                signupUser(driverName, mobileno1, mobileno2, driverEmail, idType, idNumber, vehicleReg
                        , schoolno1, schoolno2, schoolno3, vehicle_stat, pass);
            }
        });
    }

    private void signupUser(String name, String mobile1, String mobile2, String email, String id_type, String id_number, String vehicle_reg
            , String school1, String school2, String school3, String vehicle_status, String pass) {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<SignupRes> call = apiPost.createUser(name, mobile1, mobile2, email, id_type, id_number, vehicle_reg, school1, school2, school3, vehicle_status, pass, "driver");
        call.enqueue(new Callback<SignupRes>() {
            @Override
            public void onResponse(Call<SignupRes> call, Response<SignupRes> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Loading.dismiss();
                        Toast.makeText(SignupActivity.this, "Signed up Successfully", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                        intent.putExtra("signupKey", "yes");
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Loading.dismiss();
                    Toast.makeText(SignupActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SignupRes> call, Throwable t) {
                Loading.dismiss();
                Toast.makeText(SignupActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void moveToLogin(View view) {
        startActivity(new Intent(SignupActivity.this, LoginActivity.class));
        finish();
    }

    private boolean fieldErrorExist() {
        if (driver_name.getText().toString() == null || driver_name.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter your name", Toast.LENGTH_SHORT).show();
            return true;
        } else if (mobile1.getText().toString() == null || mobile1.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter your mobile no 1", Toast.LENGTH_SHORT).show();
            return true;
        } else if (id_type.getText().toString() == null || id_type.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter your id type", Toast.LENGTH_SHORT).show();
            return true;
        } else if (id_number.getText().toString() == null || id_number.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter your id number", Toast.LENGTH_SHORT).show();
            return true;
        } else if (vehicle_reg_no.getText().toString() == null || vehicle_reg_no.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter the vehicle registration number", Toast.LENGTH_SHORT).show();
            return true;
        } else if (school1.getText().toString() == null || school1.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Select first school", Toast.LENGTH_SHORT).show();
            return true;
        } else if (vehicle_status.getText().toString() == null || vehicle_status.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Select vehicle status", Toast.LENGTH_SHORT).show();
            return true;
        } else if (password.getText().toString() == null || password.getText().toString().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Enter your password", Toast.LENGTH_SHORT).show();
            return true;
        }
//        else if (driver_email.getText().toString().matches(emailpattern)) {
//            Toast.makeText(getApplicationContext(), "Incorrect email pattern", Toast.LENGTH_SHORT).show();
//            return true;
//        }
        else if (password.length() < 8) {
            Toast.makeText(getApplicationContext(), "Password too short", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

    private void getSchools() {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<SchoolsRes> call = apiPost.getSchoolList();
        call.enqueue(new Callback<SchoolsRes>() {
            @Override
            public void onResponse(Call<SchoolsRes> call, Response<SchoolsRes> response) {
                if (response.isSuccessful()) {
                    Loading.dismiss();
                    SchoolsRes resObj = response.body();
                    List<SchoolsDatum> list = resObj.getData();
                    for (SchoolsDatum datum : list) {
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SignupActivity.this,
                                android.R.layout.simple_list_item_1, Integer.parseInt(datum.getSchoolsName()));
                        school1.setAdapter(adapter);
                        school1.showDropDown();
                    }

                } else {
                    Loading.dismiss();
                    Toast.makeText(SignupActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SchoolsRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(SignupActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(SignupActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}