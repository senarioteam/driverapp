package com.example.driverapp.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.driverapp.ApiPost;
import com.example.driverapp.Loading;
import com.example.driverapp.Models.ChildRecRes;
import com.example.driverapp.Models.ChildRecord;
import com.example.driverapp.Models.Datum;
import com.example.driverapp.Models.ShowChildRes;
import com.example.driverapp.R;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PickDropActivity extends AppCompatActivity {

    Button btnPick, btnDrop, btnCancel;
    TextView parentId, childName, schoolName;
    ImageView child_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_drop);
        btnPick = findViewById(R.id.btnPick);
        btnDrop = findViewById(R.id.btnDrop);
        btnCancel = findViewById(R.id.btnCancel);
        parentId = findViewById(R.id.parentid);
        childName = findViewById(R.id.childName);
        schoolName = findViewById(R.id.schoolName);
        child_image = findViewById(R.id.imageView);
        String scanId = getIntent().getStringExtra("idKey");

        showChildRec(scanId);
        showScannedRecord();

        btnPick.setOnClickListener(v -> {
            Intent intent = new Intent(PickDropActivity.this, LocationActivity.class);
            intent.putExtra("pickKey", "Picked up");
            intent.putExtra("idNextKey", getIntent().getStringExtra("idKey"));
            startActivity(intent);
        });
        btnDrop.setOnClickListener(v -> {
            Intent intent = new Intent(PickDropActivity.this, LocationActivity.class);
            intent.putExtra("dropKey", "Dropped off");
            intent.putExtra("idNextKey", getIntent().getStringExtra("idKey"));
            startActivity(intent);
        });
        btnCancel.setOnClickListener(view -> {
            startActivity(new Intent(PickDropActivity.this, NavMainActivity.class));
        });
    }

    private void showScannedRecord() {
        Loading.show(this);
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        Call<ShowChildRes> call = apiPost.showChildRecord();
        call.enqueue(new Callback<ShowChildRes>() {
            @Override
            public void onResponse(Call<ShowChildRes> call, Response<ShowChildRes> response) {
                if (response.isSuccessful()) {
                    ShowChildRes resObj = response.body();
                    List<Datum> list = resObj.getData();
                    String scanId = getIntent().getStringExtra("idKey");
                    if (list != null) {
                        for (Datum datum : list) {
                            String ids = datum.getUserId().toString();
                            if (ids.contains(scanId)) {
                                Loading.dismiss();
                                btnPick.setVisibility(View.GONE);
                                btnDrop.setVisibility(View.VISIBLE);
                            } else {
                                Loading.dismiss();
                                btnPick.setVisibility(View.VISIBLE);
                                btnDrop.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        Loading.dismiss();
                    }

                } else {
                    Loading.dismiss();
                    Toast.makeText(PickDropActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ShowChildRes> call, Throwable t) {
                Loading.dismiss();
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(PickDropActivity.this, "Time out, Please try again", Toast.LENGTH_SHORT).show();
                } else if (t instanceof IOException) {
                    Toast.makeText(PickDropActivity.this, "Check you internet connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void showChildRec(String id) {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://sts.mobifinplus.com/api/")
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiPost apiPost = retrofit.create(ApiPost.class);
        ChildRecRes childRecRes = new ChildRecRes(id);
        Call<ChildRecRes> call = apiPost.showChildRec(childRecRes);
        call.enqueue(new Callback<ChildRecRes>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<ChildRecRes> call, Response<ChildRecRes> response) {
                if (response.isSuccessful()) {
                    ChildRecRes resObj = response.body();
                    List<ChildRecord> list = resObj.getData();
                    childName.setText("Name: " + list.get(0).getChildsName());
                    schoolName.setText("School: " + list.get(0).getSchoolsName());
                    Glide.with(PickDropActivity.this).load("https://sts.mobifinplus.com/attachments/" + list.get(0).getChildsImage()).into(child_image);
                    Loading.dismiss();

                } else {
                    Loading.dismiss();
                    Toast.makeText(PickDropActivity.this, "Process Failed", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ChildRecRes> call, Throwable t) {
                Toast.makeText(PickDropActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}