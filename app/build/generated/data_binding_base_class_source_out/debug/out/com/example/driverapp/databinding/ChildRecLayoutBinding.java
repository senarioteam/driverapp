// Generated by view binder compiler. Do not edit!
package com.example.driverapp.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import com.example.driverapp.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ChildRecLayoutBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextView createdat;

  @NonNull
  public final TextView location;

  @NonNull
  public final TextView name;

  @NonNull
  public final TextView status;

  @NonNull
  public final LinearLayout tripLin;

  @NonNull
  public final TextView tripTime;

  @NonNull
  public final View tripViewLine;

  private ChildRecLayoutBinding(@NonNull RelativeLayout rootView, @NonNull TextView createdat,
      @NonNull TextView location, @NonNull TextView name, @NonNull TextView status,
      @NonNull LinearLayout tripLin, @NonNull TextView tripTime, @NonNull View tripViewLine) {
    this.rootView = rootView;
    this.createdat = createdat;
    this.location = location;
    this.name = name;
    this.status = status;
    this.tripLin = tripLin;
    this.tripTime = tripTime;
    this.tripViewLine = tripViewLine;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ChildRecLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ChildRecLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.child_rec_layout, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ChildRecLayoutBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.createdat;
      TextView createdat = rootView.findViewById(id);
      if (createdat == null) {
        break missingId;
      }

      id = R.id.location;
      TextView location = rootView.findViewById(id);
      if (location == null) {
        break missingId;
      }

      id = R.id.name;
      TextView name = rootView.findViewById(id);
      if (name == null) {
        break missingId;
      }

      id = R.id.status;
      TextView status = rootView.findViewById(id);
      if (status == null) {
        break missingId;
      }

      id = R.id.trip_lin;
      LinearLayout tripLin = rootView.findViewById(id);
      if (tripLin == null) {
        break missingId;
      }

      id = R.id.trip_time;
      TextView tripTime = rootView.findViewById(id);
      if (tripTime == null) {
        break missingId;
      }

      id = R.id.trip_view_line;
      View tripViewLine = rootView.findViewById(id);
      if (tripViewLine == null) {
        break missingId;
      }

      return new ChildRecLayoutBinding((RelativeLayout) rootView, createdat, location, name, status,
          tripLin, tripTime, tripViewLine);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
